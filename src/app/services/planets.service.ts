import {Injectable, EventEmitter} from '@angular/core'
import {Http, Response} from '@angular/http';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Planet } from './../classes/planet';


@Injectable()
export class PlanetsService {
    
    selectedPlanet: Planet;
    selectedPlanetUpdate = new EventEmitter<Planet>();
    
    selectedPlanets: Planet[] = [];
    private _planetsUrl: string = "https://swapi.co/api/planets/?format=json";

    
    constructor(private _http: Http) { }
    
    getPlanets() {
        return this._http.get(this._planetsUrl)
            .map( (response: Response) => response.json().results )
            .catch((error: any) => {
                    if (error.status === 500) {
                        return Observable.throw(new Error(error.status));
                    }
                    else if (error.status === 400) {
                        return Observable.throw(new Error(error.status));
                    }
                    else if (error.status === 409) {
                        return Observable.throw(new Error(error.status));
                    }
                    else if (error.status === 406) {
                        return Observable.throw(new Error(error.status));
                    }
                });
    }
    
    getSelectedPlanet() {
        return this.selectedPlanet;
    }
    getSelectedPlanets() {
        return this.selectedPlanets;
    }
    
    selectPlanet(planet: Planet) {
        this.selectedPlanet = planet;
        this.selectedPlanetUpdate.emit(this.selectedPlanet);
    }
    
    addPlanetToSelectedPlanets(planet: Planet) {
        if ( this.selectedPlanets.filter(selectedPlanets => selectedPlanets === planet).length === 0 ) {
            this.selectedPlanets.push(planet);
        }
    }
    
    deletePlanetFromSelectedPlanets(selectedPlanet: Planet) {
        if ( this.selectedPlanets.filter(selectedPlanets => selectedPlanets === selectedPlanet).length === 1 ) {
//            this.selectedPlanets.pop(planet);
//            this.data.splice(this.data.indexOf(msg), 1);
            this.selectedPlanets.splice(this.selectedPlanets.indexOf(selectedPlanet), 1);
        }
    }
    
}

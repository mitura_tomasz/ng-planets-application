import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { PlanetsService } from './services/planets.service';

import { AppComponent } from './app.component';
import { ListComponent } from './components/list/list.component';
import { DescriptionComponent } from './components/description/description.component';



@NgModule({
    declarations: [
        AppComponent,
        ListComponent,
        DescriptionComponent,
    ],
    imports: [
        BrowserModule,
        HttpModule
    ],
    providers: [PlanetsService],
    bootstrap: [AppComponent]
})
export class AppModule { }

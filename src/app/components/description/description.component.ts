import { Component, OnInit, Input } from '@angular/core';

import { PlanetsService } from './../../services/planets.service';

import { Planet } from './../../classes/planet';


@Component({
    selector: 'app-description',
    templateUrl: './description.component.html',
    styleUrls: ['./description.component.scss'],
//    outputs: ['selectedPlanet'],
})
export class DescriptionComponent implements OnInit {

    @Input() selectedPlanet: Planet;
    
    constructor(private _planetsService: PlanetsService) { 
    }

    ngOnInit() {
    }
    
    
    addPlanetToSelectedPlanets(selectedPlanet: Planet) {
        this._planetsService.addPlanetToSelectedPlanets(selectedPlanet);
    }
    
    deletePlanetFromSelectedPlanets(selectedPlanet: Planet) {
        this._planetsService.deletePlanetFromSelectedPlanets(selectedPlanet);
    }
    
}


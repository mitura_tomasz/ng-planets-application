import { Component, OnInit, Input } from '@angular/core';

import { PlanetsService } from './../../services/planets.service';

import { Planet } from './../../classes/planet';


@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
    
    @Input() title: string;
    @Input() planets: Planet[];
    
    
    constructor(private _planetsService: PlanetsService) { 
    }

    ngOnInit() {
        
//        this._planetsService
//            .getPlanets()
//            .subscribe( responsePlanetsData => this.planets = responsePlanetsData );
    }
    
    onShowDescriptionClick(selectedPlanet: Planet) {
        this._planetsService.selectPlanet(selectedPlanet);
    }

}

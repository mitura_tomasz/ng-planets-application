import { Component } from '@angular/core';

import { PlanetsService } from './services/planets.service';

import { Planet } from './classes/planet';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    
    selectedPlanet: Planet;
    planets: Planet[];
    selectedPlanets: Planet[];
    
    constructor(private _planetsService: PlanetsService) { 
    }

    ngOnInit() {
        this.selectedPlanet = this._planetsService.getSelectedPlanet();
        this.selectedPlanets = this._planetsService.getSelectedPlanets();
        this._planetsService.selectedPlanetUpdate.subscribe(
            (selectedPlanet) => { 
                this.selectedPlanet = this._planetsService.getSelectedPlanet();
            }
        );
        
        this._planetsService
            .getPlanets()
            .subscribe( responsePlanetsData => this.planets = responsePlanetsData );
    }
    


    onShowDescriptionClick(selectedPlanet: Planet) {
        this._planetsService.selectPlanet(selectedPlanet);
    }
    
    
}
